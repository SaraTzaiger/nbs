const express = require('express');
const router = express.Router();

const distanceCtrl = require('../controllers/distanceCtrl');

router.get('/:source/:destination', distanceCtrl.getDistance);
router.delete('/:source/:destination', distanceCtrl.deleteDistance);
router.get('/popular-search', distanceCtrl.getPopularSearch);
router.get('/popular-search-list', distanceCtrl.getPopularSearchList);

module.exports = router;
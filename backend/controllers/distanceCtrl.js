const Distance = require('../models/distanceMdl');
const request = require('request');

exports.createDistance = (req, res, next) => {
  var url = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=km&origins='+encodeURIComponent(req.params.source)+'&destinations='+encodeURIComponent(req.params.destination)+'&key=AIzaSyBxvqGxEvb6ZBnyRTM8isBU_6O-MAfuNiQ';
  request(url, { json: true }, (err, response, body) => {
    if (err) {   
      console.log(err);     
      res.status(400).json({
      error: err
    });
   }else{
    console.log(response.body.rows[0].elements[0].distance.text);
    var dist = response.body.rows[0].elements[0].distance.text;

    const distance = new Distance({
      source: req.params.source,
      destination: req.params.destination,
      distance: dist,
      cnt: 1
    });

    distance.save().then(
      (x) => {
        console.log("saved ", x);
        res.status(201).json( dist );
      }
    ).catch(
      (error) => {
        res.status(400).json({
          error: error
        });
      }
    );
   }
  });

};

exports.getOneDistance = (req, res, next) => {
  Distance.findOne({
    _id: req.params.id
  }).then(
    (distance) => {
      res.status(200).json(distance);
    }
  ).catch(
    (error) => {
      res.status(404).json({
        error: error
      });
    }
  );
};

exports.modifyDistance = (req, res, next) => {
  const distance = new Distance({
    _id: req.params.id,
    title: req.body.title,
    description: req.body.description,
    imageUrl: req.body.imageUrl,
    price: req.body.price,
    userId: req.body.userId
  });
  Distance.updateOne({source: req.params.source, destination: req.params.destination}, distance).then(
    () => {
      res.status(201).json({
        message: 'Distance updated successfully!'
      });
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.cntPopular = (req, res, next, cnt) => {
  console.log("cntPopular", cnt);
  Distance.updateOne({source: req.params.source, destination: req.params.destination}, { $set: {cnt: cnt} }).then(
    () => {
      next();
    }
  ).catch(
    (error) => {
      console.log("400", error);
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.deleteDistance = (req, res, next) => {
  Distance.deleteOne({source: req.params.source, destination: req.params.destination}).then(
    () => {
      res.status(200).json({
        message: 'Deleted!'
      });
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.getDistance = (req, res, next) => {
  console.log("req.source, destination: req.destination", req.params.source, req.params.destination)
  Distance.find({source: req.params.source, destination: req.params.destination}).then(
    (distance) => {
      console.log("get distance: ", distance);
      if(distance.length == 0){
          this.createDistance(req, res, next);
      }else{
        var cnt = distance[0].cnt + 1;
        this.cntPopular(req, res, function(){
          console.log("return");
          res.status(200).json(
            distance[0].distance
         );
        }, cnt);

      }

    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.getPopularSearch = (req, res, next) => {
  console.log("getPopularSearch")
  Distance.find({}).sort('-cnt').limit(1).then(
    (distance) => {
      console.log("Popular: ", distance);
      res.status(200).json(
        distance[0]
      );
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.getPopularSearchList = (req, res, next) => {
  console.log("getPopularSearch")
  Distance.find({}).sort('-cnt').limit(5).then(
    (distance) => {
      console.log("Popular: ", distance);
      res.status(200).json(
        distance
      );
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};
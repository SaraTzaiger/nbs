const mongoose = require('mongoose');

const distanceSchema = mongoose.Schema({
  source: { type: String, required: true },
  destination: { type: String, required: true },
  distance: { type: String, required: true },
  cnt: {type: Number, required: false, default: 0}
});

module.exports = mongoose.model('Distance', distanceSchema);
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DistanceService {
  configUrl: string;

  constructor(private http: HttpClient) { 
    this.configUrl = "http://localhost:3001";
  }

  getDistance(source, destination): any {
    return this.http.get(this.configUrl+"/distance/"+source+"/"+destination);
  }

  deleteDistance(source, destination): any {
    return this.http.delete(this.configUrl+"/distance/"+source+"/"+destination);
  }

  getPopularsearch(): any {
    return this.http.get(this.configUrl+"/distance/popular-search");
  }

  getPopularsearchList(): any {
    return this.http.get(this.configUrl+"/distance/popular-search-list");
  }

}

import { Component, OnInit, Input } from '@angular/core';
import { DistanceService } from 'src/app/services/distance.service';
import { Distance } from 'src/app/models/Distance.model';


@Component({
  selector: 'app-distance',
  templateUrl: './distance.component.html',
  styleUrls: ['./distance.component.css']
})
export class DistanceComponent implements OnInit {

  distance: String;
  source: String;
  destination: String;
  popularDistance: Distance;
  showResDistance: boolean;
  showResPopular: boolean;
  popularDistanceList: Distance[];
  showResPopularList: boolean;
 


  constructor(private distanceService: DistanceService) { }

  ngOnInit() {
    // debugger;
    this.source = "jerusalem";
    this.destination = "bnei-brak";
    this.distance = "";
    this.popularDistance = new Distance();
    this.showResDistance = false;
    this.showResPopular = false;
  }

  displayDistance(){
    debugger;
    this.distanceService.getDistance(this.source, this.destination).subscribe((distance: String)=>{
        this.distance = distance;
        this.showResDistance = true;
    },
    (error)=>{
      alert("error accurrd");// JSON.stringify(error)
    });
  }

  getPopularsearch(){
    this.distanceService.getPopularsearch().subscribe((distance: Distance)=>{
      this.popularDistance = distance;
      this.showResPopular = true;
    },
    (error)=>{
      alert("error accurrd");// JSON.stringify(error)
    })
  }

  getPopularsearchList(){
    this.distanceService.getPopularsearchList().subscribe((distance: Distance[])=>{
      this.popularDistanceList = distance;
      this.showResPopularList = true;
    },
    (error)=>{
      alert("error accurrd");// JSON.stringify(error)
    })
  }

  // deleteDistance

}
export class Distance {
    _id: string;
    source: string;
    destination: string;
    distance: string;
    cnt: Number;
  }
  